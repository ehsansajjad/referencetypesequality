﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTypesEquality
{
class Program
{
    static void Main(string[] args)
    {
        Car carA = new Car(2018, 100000);
        Car carA2 = new Car(2018, 100000);
        Car carB = new Car(2016, 2500000);
        LeaseCar leasedCarA = new LeaseCar(2014, 2500000, new LeaseOptions(1000000, InstallmentPlan.FiveYears, 10000));
        LeaseCar leasedCarA2 = new LeaseCar(2014, 2500000, new LeaseOptions(1000000, InstallmentPlan.FiveYears, 10000));
        LeaseCar leasedCarB = new LeaseCar(2016, 2500000, new LeaseOptions(1000000, InstallmentPlan.FiveYears, 10000));

        Console.WriteLine(carA == carA2);
        Console.WriteLine(carA == carB);
        Console.WriteLine(leasedCarA == leasedCarA);
        Console.WriteLine(leasedCarB == carB);

        Console.Read();
    }
}
}


