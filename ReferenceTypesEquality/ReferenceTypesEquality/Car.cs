﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTypesEquality
{
    public class Car
    {
        public int MakeYear { get; private set; }
        public Decimal Price { get; private set; }

        public Car(int makeYear, Decimal price)
        {
            MakeYear = makeYear;
            Price = price;
        }

        public override string ToString()
        {
            return $"Model: {MakeYear}, Price: {Price}";
        }


        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(obj, this))
                return true;

            if (obj.GetType() != this.GetType())
                return false;

            Car carToCheck = obj as Car;

            return this.MakeYear == carToCheck.MakeYear
                && this.Price == carToCheck.Price;
        }

        public override int GetHashCode()
        {
            return this.MakeYear.GetHashCode() ^ this.Price.GetHashCode();
        }

        public static bool operator ==(Car carA, Car carB)
        {
            return Object.Equals(carA, carB);
        }

        public static bool operator !=(Car carA, Car carB)
        {
            return !Object.Equals(carA, carB);
        }



    }
}

