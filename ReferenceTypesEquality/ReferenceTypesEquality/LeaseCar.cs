﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReferenceTypesEquality
{
    public class LeaseCar : Car
    {

        public LeaseOptions Options { get; set; }

        public LeaseCar(int makeYear, Decimal price, LeaseOptions options) : base(makeYear, price)
        {
            Options = options;
        }

        public override string ToString()
        {
            return $"Model: {MakeYear}, Price: {Price}, Lease Options: {Options.ToString()}";
        }

        public override bool Equals(object obj)
        {
            if (!base.Equals(obj))
                return false;
            LeaseCar objectToCompare = (LeaseCar)obj;
            return this.Options == objectToCompare.Options;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ this.Options.GetHashCode();
        }

        public static bool operator ==(LeaseCar carA, LeaseCar carB)
        {
            return object.Equals(carA, carB);
        }

        public static bool operator !=(LeaseCar carA, LeaseCar carB)
        {
            return !object.Equals(carA, carB);
        }
    }

    public class LeaseOptions
    {
        public Decimal DownPayment { get; }
        public InstallmentPlan InstallmentPlan { get; }
        public Decimal MonthlyInstallment { get; }

        public LeaseOptions(Decimal downPayment, InstallmentPlan installmentPlan, Decimal monthlyInstallment)
        {
            DownPayment = downPayment;
            InstallmentPlan = InstallmentPlan;
            MonthlyInstallment = monthlyInstallment;
        }




        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;

            if (ReferenceEquals(obj, this))
                return true;

            if (obj.GetType() != this.GetType())
                return false;

            LeaseOptions optionsToCheck = obj as LeaseOptions;

            return this.DownPayment == optionsToCheck.DownPayment
                && this.InstallmentPlan == optionsToCheck.InstallmentPlan
                && this.MonthlyInstallment == optionsToCheck.MonthlyInstallment;
        }
        public override int GetHashCode()
        {
            return this.DownPayment.GetHashCode()
                    ^ this.InstallmentPlan.GetHashCode()
                    ^ this.MonthlyInstallment.GetHashCode();
        }

        public static bool operator ==(LeaseOptions optionsA, LeaseOptions optionsB)
        {
            return Object.Equals(optionsA, optionsB);
        }

        public static bool operator !=(LeaseOptions optionsA, LeaseOptions optionsB)
        {
            return !Object.Equals(optionsA, optionsB);
        }

        public override string ToString()
        {
            return $"{nameof(DownPayment)}: {DownPayment}, {nameof(InstallmentPlan)}: {InstallmentPlan.ToString()}, {nameof(MonthlyInstallment)}: {MonthlyInstallment}";
        }


    }


    public enum InstallmentPlan
    {
        OneYear,
        ThreeYears,
        FiveYears
    }
}
